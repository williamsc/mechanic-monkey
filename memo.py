class WordleSolver:
    def __init__(self, word, wordlist):
        self.word = word
        self.word_map = ['abcdefghijklmnopqrstuvwxyz']*len(word)
        self.word_rules = {}
        self.cur_word_list = wordlist
        self.bcolors = self.Bcolors()
    def __str__(self):
        print(' --- '.join(self.solve()))
    class Bcolors:
        MATCH = '\033[92m'
        NEARBY = '\033[93m'
        ENDC = '\033[0m'
    def check_word(self, word):
        # Checking against word map updates
        for i, letter in enumerate(word):
            if letter not in self.word_map[i]:
                return False
        # Failing that, check against word rules NEARBY/MATCH
        for letter in self.word_rules:
            chk=False
            for idx in self.word_rules[letter]:
                if word[int(idx)]==letter:
                    chk=True
            if chk==False:
                return False
        return True
    def update_dictionary(self):
        newlist = []
        for word in self.cur_word_list:
            if (self.check_word(word)):
                newlist.append(word)
        return newlist
    def wordheat(self, word):
        score = 0
        for l in ''.join(set(word)):
            score += self.heatmap[l]
        return score
    def build_wordheat_map(self):
        self.heatmap = {}
        for l in 'abcdefghijklmnopqrstuvwxyz':
            self.heatmap[l] = len([x for x in self.cur_word_list if l in x])
        heatlist = {}
        for word in self.cur_word_list:
            heatlist[word] = self.wordheat(word)
        return sorted(heatlist.items(), key=lambda x:x[1])
    def update_word_map(self, guess):
        for i, row in enumerate(guess):
            letter, prox = row
            if prox == "NEARBY":
                self.word_map[i] = self.word_map[i].replace(letter,"")
                self.word_rules[letter] = "01234".replace(str(i),"")
            elif prox == "NONE":
                #if this letter didnt already exist in string
                for idx, map in enumerate(self.word_map):
                    self.word_map[idx] = self.word_map[idx].replace(letter,"") if len(self.word_map[idx].replace(letter,""))>0 else self.word_map[idx]
            else:
                self.word_map[i]=letter
                self.word_rules[letter]=str(i)
        self.cur_word_list = self.update_dictionary() 
        print(f"w.update_word_map(["+','.join([f"('{l}','{'MATCH' if ''.join(self.word_map[idx])==l else 'UNK'}')" for idx, l in enumerate(self.build_wordheat_map()[-1][0])])+"])")
        return self.build_wordheat_map()[-1][0]      
    def word_validate(self, guess):
        resp = ""
        result = []
        if len(self.word) == len(guess):
            for idx, letter in enumerate(guess):
                if letter == word[idx]:
                    resp += f"{self.bcolors.MATCH}{letter}{self.bcolors.ENDC}"
                    result.append((letter, "MATCH"))
                elif letter in word:
                    resp += f"{self.bcolors.NEARBY}{letter}{self.bcolors.ENDC}"
                    result.append((letter, "NEARBY"))
                else:
                    resp += letter
                    result.append((letter, "NONE"))
        return resp, result, self.update_word_map(result)
    def solve(self):
        resp, result, next = self.word_validate(self.build_wordheat_map()[-1][0])
        #print(resp)
        prev=resp
        full_resp=[resp]
        while (len([f[1] for f in result if(f[1] not in 'MATCH')])>0):
            resp, result, next = self.word_validate(next)
            if resp==prev:
                print("seems stuck")
                break
            prev=resp
            full_resp.append(resp)
            #print(resp)   
        return full_resp

def get_dictionary(word_len=0, partofspeech=None):
    dic = []
    with open("dictionary/words.txt", 'r') as infile:
        for line in infile.readlines():
            word = line.split(" ")[0].replace("+","").replace("-","")
            part = line.split(" ")[1].split(":")[0]
            if word not in dic and (word_len == 0 or len(word) == word_len) and (partofspeech is None or part == partofspeech):
                dic.append(word)
    return dic


word_list = get_dictionary(word_len=5)
for word in word_list:
    w=WordleSolver(word, word_list).solve()
    print(f"{w[-1]}: {len(w)} attempts")


#Test use case
w = WordleSolver('angry',word_list)
w.update_word_map([('o','NONE'),('r','NONE'),('a','NONE'),('t','NONE'),('e','MATCH')])
w.update_word_map([('s','NONE'),('l','NEARBY'),('i','NONE'),('d','NONE'),('e','MATCH')])
w.update_word_map([('b','NONE'),('u','MATCH'),('l','NEARBY'),('g','MATCH'),('e','MATCH')])