# Mechanic Monkey



## Getting started

This script will analyze a wordle puzzle based on user input, and attempt to provide a list of remaining valid words. Not all words will be valid, as this dictionary is based on Merriam-Webster and might be more complete than wordle.

It would be cool if i identified some more common letters and priorized guesses based on that info, and if it had a more useful process for users to pass along guess data. Currently its just run within a python shell.

## Authors and acknowledgment
I made this on a slow Friday morning

## License
MIT License seems cool

## Project status
Its functional, perhaps I will make it more useful or interactive later.
